
<%@ page isELIgnored="false" %>
<%--
  Created by IntelliJ IDEA.
  User: Khomenko_D
  Date: 31.03.2016
  Time: 23:02
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>


<html>
<head>
    <title>Pizza Online</title>

    <link rel="stylesheet" href="css/main_page_style.css">
    <meta charset='utf-8'>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="styles.css">
    <script src="http://code.jquery.com/jquery-latest.min.js" type="text/javascript"></script>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>

    <%--<!-- Compiled and minified CSS -->--%>
    <%--<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.6/css/materialize.min.css">--%>

    <%--<!-- Compiled and minified JavaScript -->--%>
    <%--<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.6/js/materialize.min.js"></script>--%>


    <script src="script.js"></script>
    <title>CSS MenuMaker</title>
</head>
<body>
<table class="cart">
    </table>




<div id="header">



        <div class="navigation">

                <div class="top">
                    <h1 id="title" class="hidden"><span id="logo">Pizza <span>Online</span></span></h1>

                </div>


                <div class="dropdown">
                    <button class="dropbtn">Koszyk</button>
                    <div class="dropdown-content">
                        <h3>Pizza w koszyku</h3>



                        <%--<table class="cart">--%>

                        <%--&lt;%&ndash;<tr >&ndash;%&gt;--%>
                            <%--&lt;%&ndash;<td>&ndash;%&gt;--%>
                                <%--&lt;%&ndash;text&ndash;%&gt;--%>
                            <%--&lt;%&ndash;</td>&ndash;%&gt;--%>
                            <%--&lt;%&ndash;<td><input class=myquantity type="number" min="1" max="5" value="1"/></td>&ndash;%&gt;--%>
                            <%--&lt;%&ndash;<td class="rozmiar">&ndash;%&gt;--%>
                                <%--&lt;%&ndash;rozmiar&ndash;%&gt;--%>
                            <%--&lt;%&ndash;</td>&ndash;%&gt;--%>
                            <%--&lt;%&ndash;<td>&ndash;%&gt;--%>
                                <%--&lt;%&ndash;<button class="deletebutton">Usun</button>&ndash;%&gt;--%>
                            <%--&lt;%&ndash;</td>&ndash;%&gt;--%>
                        <%--&lt;%&ndash;</tr>&ndash;%&gt;--%>
                            <%--&lt;%&ndash;<tr >&ndash;%&gt;--%>

                                <%--&lt;%&ndash;<td>&ndash;%&gt;--%>
                                    <%--&lt;%&ndash;text&ndash;%&gt;--%>
                                <%--&lt;%&ndash;</td>&ndash;%&gt;--%>
                                <%--&lt;%&ndash;<td><input class=myquantity type="number" min="1" max="5" value="1"/></td>&ndash;%&gt;--%>
                                <%--&lt;%&ndash;<td class="rozmiar">&ndash;%&gt;--%>
                                    <%--&lt;%&ndash;rozmiar&ndash;%&gt;--%>
                                <%--&lt;%&ndash;</td>&ndash;%&gt;--%>
                                <%--&lt;%&ndash;<td>&ndash;%&gt;--%>
                                    <%--&lt;%&ndash;<button class="deletebutton">Usun</button>&ndash;%&gt;--%>
                                <%--&lt;%&ndash;</td>&ndash;%&gt;--%>
                            <%--&lt;%&ndash;</tr>&ndash;%&gt;--%>

                        <%--</table>--%>


                        <button class="buybutton">Kupić</button>
                    </div>
                </div>
        </div>


        <div class="pizzas_menu">
            <br/>
            <div id="flip">Stwórz własną pizze</div>

            <div id="panel">

                <table id="customers1">
                    <tr>
                        <th>Rozmiar</th>
                        <th></th>
                        <th></th>
                        <th></th>

                    </tr>
                    <tr>
                        <td><input type="radio" name="size" value="0" checked="true"/> Średnia 26 cm</td>
                        <td><input type="radio" name="size" value="1"/> Duża 36 cm</td>
                        <td><input type="radio" name="size" value="2"/> Gigant 50 cm</td>
                    </tr>
                    <tr>
                </table>


                <table id="customers2">
                    <tr>
                        <th>Składniki</th>
                        <th></th>
                        <th></th>
                        <th></th>
                    </tr>
                    <tr>

                        <c:forEach items="${ingredients}" var="ingredient" varStatus="loop">
                        <c:if test="${not loop.first and loop.index % 4 == 0}">
                    </tr>
                    <tr>
                        </c:if>
                        <td><c:out value="${ingredient.name}"/> &nbsp; <input name="ingred" type="number" min="0" max="2"
                                                                      value="0"/></td>
                        </c:forEach>


                </table>


                <table class="pizzas_menu_footer" border="0" width="100%">
                    <tr>
                        <td>Ilość<input name="quantity" type="number" min="1" max="10" value="1"/></td>
                        <td width="60">
                            <button class="button" id="onClick" onclick="">Do koszyka</button>
                        </td>
                    </tr>
                </table>
            </div>


            <div id="pizzas_menu">

                <div id="box1">
                    <% for (int i = 0; i < 10; i++) {
                        out.println("<br/>");
                    } %>


                    <table id="customers">
                        <caption></caption>
                        <tr>
                            <th>Pizza</th>
                            <th>Składniki</th>
                            <th>Ilość</th>
                            <th>Rozmiar</th>
                            <th></th>
                        </tr>

                        <c:forEach items="${complitedpizza}" var="pizza" varStatus="loop">
                            <tr class="comppizza" >
                                <td class="comppizzatd"><h3><c:out value="${pizza.name}"/></h3></td>
                                <td class="comppizzatd">
                                    <c:forEach items="${pizza.ingredients}" var="ingredient" varStatus="loop">
                                        <c:out value="${ingredient.name}"/>
                                        <c:out value="${not loop.last?',':''}"/>

                                    </c:forEach>
                                </td class="comppizzatd">
                                <td class="comppizzatd"><input class=myquantity type="number" min="1" max="5" value="1"/></td>
                                <td class="comppizzatd">

                                    <select class="myselect">
                                        <option value="0">Średnia 26 cm</option>
                                        <option value="1">Duża 36 cm</option>
                                        <option value="2">Gigant 50 cm</option>
                                    </select>

                                </td>
                                <td>
                                    <button  class="compbutton">Do koszyka</button>
                                </td>
                            </tr>
                        </c:forEach>

                    </table>
                </div>
            </div>
        </div>

</div>


</body>
</html>
