<%--
  Created by IntelliJ IDEA.
  User: Khomenko_D
  Date: 29.03.2016
  Time: 16:36
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>

<head>
  <meta charset="utf-8">
  <title>Pizza Online</title>

  <!-- Google Fonts -->
  <link href='https://fonts.googleapis.com/css?family=Roboto+Slab:400,100,300,700|Lato:400,100,300,700,900' rel='stylesheet' type='text/css'>

  <link rel="stylesheet" href="css/animate.css">
  <!-- Custom Stylesheet -->
  <link rel="stylesheet" href="css/style.css">

  <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
</head>

<body>
<%--<div class="container">--%>
  <%--<div class="top">--%>
    <%--<h1 id="title" class="hidden"><span id="logo">Pizza <span>Online</span></span></h1>--%>
  <%--</div>--%>
  <%--<div class="login-box animated fadeInUp">--%>
    <%--<div class="box-header">--%>
      <%--<h2>Log In</h2>--%>
    <%--</div>--%>
    <%--<label for="username">Username</label>--%>
    <%--<br/>--%>
    <%--<input type="text" id="username">--%>
    <%--<br/>--%>
    <%--<label for="password">Password</label>--%>
    <%--<br/>--%>
    <%--<input type="password" id="password">--%>
    <%--<br/>--%>
    <%--<button type="submit">Sign In</button>--%>
    <%--<br/>--%>
    <%--<a href="#"><p class="small">Forgot your password?</p></a>--%>
  <%--</div>--%>
<%--</div>--%>



</body>

<script>
  $(document).ready(function () {
    $('#logo').addClass('animated fadeInDown');
    $("input:text:visible:first").focus();
  });
  $('#username').focus(function() {
    $('label[for="username"]').addClass('selected');
  });
  $('#username').blur(function() {
    $('label[for="username"]').removeClass('selected');
  });
  $('#password').focus(function() {
    $('label[for="password"]').addClass('selected');
  });
  $('#password').blur(function() {
    $('label[for="password"]').removeClass('selected');
  });
</script>

</html>
