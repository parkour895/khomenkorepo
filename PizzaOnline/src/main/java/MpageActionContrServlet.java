import business.*;
import com.google.gson.Gson;
import data.Query;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


/**
 * Created by Khomenko_D on 25.05.2016.
 */

@WebServlet("/mainpagecontrol")
public class MpageActionContrServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        int counter = 0;


        HttpSession session=req.getSession();
        Cart cart = (Cart) session.getAttribute("cart");


        if (cart == null) {
            cart = new Cart();
            session.setAttribute("cart", cart);
        }

            if(req.getParameter("deletepizzaid")!=null){
                int pizzaId =Integer.parseInt(req.getParameter("deletepizzaid"));
                cart.getItems().remove(pizzaId);
            }

            if (req.getParameter("quantity") != null) {
                //&& req.getParameter("ingredient[]") != null

                LineItem lineItem = new LineItem();

                MyPizza myPizza = new MyPizza();

                int pizzaQuantity=Integer.parseInt(req.getParameter("quantity"));

                lineItem.setQuantity(pizzaQuantity);

                int pizzaSize=Integer.parseInt(req.getParameter("size"));

                myPizza.setSize(pizzaSize);

                float pizzaBasePrice=Query.getPizzaBasePriceByIdSize(pizzaSize+1);

                myPizza.setBasePrice(pizzaBasePrice);


                if (req.getParameter("ingredient[]") != null) {

                    List<Ingredient> ingredientList = new ArrayList<Ingredient>();
                    String[] ingredientsQuantity = req.getParameterValues("ingredient[]");

                    ArrayList<Ingredient> ingredientsFromDB=Query.getIngredients();

                    for (int i = 0; i < ingredientsQuantity.length; i++) {
                        int quantity = Integer.parseInt(ingredientsQuantity[i]);
                        if (quantity != 0) {

                            Ingredient ingredient= ingredientsFromDB.get(i);

                            ingredient.setQuantity(quantity);

                            float ingredientPrice=ingredient.getPrice();

                            ingredient.setPrice(ingredientPrice*quantity);

                            ingredientList.add(ingredient);

                        }
                    }



                    myPizza.setIngredients(ingredientList);//add ingredients to MyPizza

                    myPizza.calculatePrice();
                }

                if(req.getParameter("pizzaid")!=null){
                    int pizzaId= Integer.parseInt(req.getParameter("pizzaid"));

                    myPizza=Query.getCompletedPizzaById(pizzaId+1);
                    myPizza.setSize(pizzaSize+1);
                    myPizza.setBasePrice(pizzaBasePrice);
                    myPizza.calculatePrice();

                }

                lineItem.setMyPizza(myPizza);
                cart.addItem(lineItem);
                session.setAttribute("cart", cart);
            }





        System.out.println(cart.getItems());



    }



    private String getJsonCart(Cart cart){
        Gson gson = new Gson();
        String jsonCart=gson.toJson(cart);

        return jsonCart;
    }
}
