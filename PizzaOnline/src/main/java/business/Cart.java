package  business;

import com.google.gson.Gson;

import javax.swing.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Khomenko_D on 08.04.2016.
 */
public class Cart implements Serializable{
    private List<LineItem> items;

    public Cart() {
        items = new ArrayList<LineItem>();
    }

    public void addItem(LineItem item)
    {
//        String name=item.getMyPizza().getName();
//        int quantity=item.getQuantity();
//        for(int i=0; i<items.size(); i++)
//        {
//            LineItem lineItem=items.get(i);
//            if(lineItem.getMyPizza().getName().equals(name)){
//                lineItem.setQuantity(quantity);
//                return;
//            }
//        }
        items.add(item);
    }

    public void removeItem(LineItem item){
        String name=item.getMyPizza().getName();
        for (int i=0; i<items.size(); i++)
        {
            if(items.get(i).getMyPizza().getName().equals(name)){
                items.remove(i);
                return;
            }

        }
    }

    public List<LineItem> getItems() {
        return items;
    }

    @Override
    public String toString() {
       int size=items.size();
        return String.valueOf(size);
    }


    public String getJsonCart(){
        Gson gson = new Gson();
        String jsonCart=gson.toJson(this);

        return jsonCart;
    }
}
