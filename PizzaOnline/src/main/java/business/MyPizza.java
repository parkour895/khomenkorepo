package  business;

import com.google.gson.Gson;
import data.Query;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Khomenko_D on 04.04.2016.
 */
public class MyPizza implements Serializable{
    private  int id=0;
    private String name="";
    private int size=0;
    private float basePrice=0;
    private float totalPrice=0;
    private List<Ingredient> ingredients=new ArrayList<Ingredient>();

    public MyPizza() {

    }

    public  MyPizza(int pizzaSize ){
        size=pizzaSize;
        float pizzaBasePrice = Query.getPizzaBasePriceByIdSize(pizzaSize);
        setBasePrice(pizzaBasePrice);
    }

    public static MyPizza createCompletedPizzaByIDandSize(int pizzaID, int pizzaSize ){
        MyPizza myPizza=Query.getCompletedPizzaById(pizzaID);
        myPizza.setSize(pizzaSize);
        myPizza.size=pizzaSize;
        float pizzaBasePrice = Query.getPizzaBasePriceByIdSize(pizzaSize);
        myPizza.setBasePrice(pizzaBasePrice);
        myPizza.calculatePrice();
        return  myPizza;
    }

    public float getBasePrice() {
        return basePrice;
    }

    public void setBasePrice(float basePrice) {
        this.basePrice = basePrice;
    }
    public List<Ingredient> getIngredients() {
        return ingredients;
    }

    public void setIngredients(List<Ingredient> ingredients) {
        this.ingredients = ingredients;
    }

    public void setIngredients(String[] ingredientsQuantity){//

        ArrayList<Ingredient> ingredientsFromDB= Query.getIngredients();

        for (int i = 0; i < ingredientsQuantity.length; i++) {
            int quantity = Integer.parseInt(ingredientsQuantity[i]);
            if (quantity != 0) {

                Ingredient ingredient= ingredientsFromDB.get(i);

                ingredient.setQuantity(quantity);

                float ingredientPrice=ingredient.getPrice();

                ingredient.setPrice(ingredientPrice*quantity);

                ingredients.add(ingredient);

                calculatePrice();
            }
        }
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }


    public float getPrice() {
        return totalPrice;
    }

    public void setPrice(float price) {
        this.totalPrice = price;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void calculatePrice(){
        for (Ingredient tempIngred:
             ingredients) {
            totalPrice+=tempIngred.getPrice();
        }
        totalPrice+=basePrice;
    }

    public String toJSON(){
        Gson gson=new Gson();
        String jsonPizza=gson.toJson(this);
        return jsonPizza;
    }


}
