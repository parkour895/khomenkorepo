package  business;

import com.google.gson.Gson;
import sun.security.jgss.GSSCaller;

import java.io.Serializable;
import java.text.NumberFormat;

/**
 * Created by Khomenko_D on 08.04.2016.
 */
public class LineItem implements Serializable{
    private MyPizza myPizza;
    private int quantity;
    private float totalPrice=0;


    public LineItem() {
    }


    public LineItem(int quantity)
    {
        this.quantity=quantity;
    }

    public MyPizza getMyPizza() {
        return myPizza;
    }

    public void setMyPizza(MyPizza myPizza) {
        this.myPizza = myPizza;
        setTotalPrice();

    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public void setTotalPrice() {
        boolean isPossibleToGetTotalPrice=(quantity!=0)&&(myPizza!=null);
        if(isPossibleToGetTotalPrice)
        totalPrice= myPizza.getPrice()*quantity;


    }

    public float getTotalPrice() {
        return totalPrice;
    }

    public String toJSON()
    {
        Gson gson=new Gson();
        return gson.toJson(this);
    }

}
