package business;

import java.io.Serializable;

/**
 * Created by Khomenko_D on 20.04.2016.
 */
public class Ingredient implements Serializable {
    int id=0;
    String name="";
    float price=0;
    int quantity=1;

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public int getQuantity() {
        return quantity;

    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
