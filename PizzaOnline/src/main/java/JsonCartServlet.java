import business.Cart;
import com.google.gson.Gson;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Created by Khomenko_D on 17.04.2016.
 */
@WebServlet("/cartjson")
public class JsonCartServlet extends HttpServlet{
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        HttpSession session=req.getSession();

        Cart cart= (Cart) session.getAttribute("cart");

        String jsonCart=cart.getJsonCart();

        resp.setContentType("application/json");//return json to jsp
        resp.setCharacterEncoding("UTF-8");//
        resp.getWriter().write(jsonCart);//

        System.out.print(jsonCart);
    }



}
