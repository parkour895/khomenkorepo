import business.Ingredient;

import business.MyPizza;
import data.Query;



import javax.servlet.ServletException;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


/**
 * Created by Khomenko_D on 08.04.2016.
 */

@WebServlet("/mainpage")
public class CartServlet extends HttpServlet {



    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        if (req.getAttribute("ingredients") == null) {
            List<Ingredient> ingredients = Query.getIngredients();
            req.setAttribute("ingredients", ingredients);
            // req.setAttribute("name", name); // Store products in request scope.

            ArrayList<MyPizza> myPizzas=Query.getAllCompletedPizza();//get all completed pizza from database
            req.setAttribute("complitedpizza", myPizzas);//save as Attribute

            req.getRequestDispatcher("/mainpage.jsp").forward(req, resp); // Forward to JSP page to display them in a HTML table.
        }




            }



    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        super.doPost(req, resp);
        req.getAttribute("ingredients");

    }




}
