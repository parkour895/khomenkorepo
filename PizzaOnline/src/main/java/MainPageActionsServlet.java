import business.Cart;
import business.Ingredient;
import business.LineItem;
import business.MyPizza;
import data.Query;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Khomenko_D on 03.06.2016.
 */
@WebServlet("/lineitem")
public class MainPageActionsServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        if (req.getParameter("quantity") != null) {
            //&& req.getParameter("ingredient[]") != null
            int pizzaQuantity = Integer.parseInt(req.getParameter("quantity"));

            LineItem lineItem = new LineItem(pizzaQuantity);


            String parametr=req.getParameter("size");

            int pizzaSize = Integer.parseInt(parametr)+1;

            if (req.getParameter("ingredient[]") != null) {
                MyPizza newPizza = new MyPizza(pizzaSize);

                String[] ingredientsQuantity = req.getParameterValues("ingredient[]");

                newPizza.setIngredients(ingredientsQuantity);

                lineItem.setMyPizza(newPizza);


            }

            if (req.getParameter("pizzaid") != null) {

                int pizzaId = Integer.parseInt(req.getParameter("pizzaid"))+1;

                MyPizza completedPizza=MyPizza.createCompletedPizzaByIDandSize(pizzaId, pizzaSize);

                lineItem.setMyPizza(completedPizza);

            }



            String jsonLineItem = lineItem.toJSON();

            resp.setContentType("application/json");//return json to jsp
            resp.setCharacterEncoding("UTF-8");//
            resp.getWriter().write(jsonLineItem);//

            System.out.print(jsonLineItem);
        }


    }

}

