package  data;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;

import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

/**
 * Created by Khomenko_D on 10.04.2016.
 */
public class DAOdatabase {



    static Connection connection;



    public static Connection sqlConnection()  {

        Properties properties=new Properties();

        ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
        InputStream input = classLoader.getResourceAsStream("db.properties");



        try {
            properties.load(input);
        } catch (IOException e) {
            System.out.println("File db.properties not found or cant by opened.");
            e.printStackTrace();
        }
        String username=properties.getProperty("user");
        String password=properties.getProperty("password");
        String dbUrl=properties.getProperty("url");


        try {
            try {
                Class.forName("com.mysql.jdbc.Driver").newInstance();
            } catch (InstantiationException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
            System.out.print("OK com.mysql.jdbc.Driver++++++++++++");
        } catch (ClassNotFoundException e) {
            System.out.println("Error : class not found ");
            e.printStackTrace();
        }

        try {
            connection=DriverManager.getConnection(dbUrl,username,password);
            System.out.println("DB jdbc:mysql://localhost:3306/pizzaonline Connected ++++++++");
        } catch (SQLException e) {
            System.out.println("Error: cant get connection.");
            e.printStackTrace();
        }


        return connection;
    }



}
