package data;

import business.Ingredient;
import business.MyPizza;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


/**
 * Created by Khomenko_D on 11.04.2016.
 */
public class Query {


    static private Connection connection;
    static private PreparedStatement preparedStatement;
    //    public static ArrayList<MyPizza> get_completed_pizza() throws SQLException {
//
//        ArrayList<MyPizza> pizzas=new ArrayList<MyPizza>();
//        try {
//
//            connection= DAOdatabase.sqlConnection();
//            preparedStatement=connection.prepareStatement("SELECT name, pizza_id FROM  my_pizza");
//            ResultSet resultSet= preparedStatement.executeQuery();
//            while(resultSet.next())
//            {
//                String name=resultSet.getString("name");
//                int pizzaId=resultSet.getInt("pizza_id");
//                ArrayList<Integer> ingredients_id= get_pizza_ingredients(pizzaId);
//                pizzas.add(new MyPizza(name, ingredients));
//            }
//
//
//        } catch (Exception e) {
//            e.printStackTrace();
//        }finally{
//            //finally block used to close resources
//            try{
//                if(preparedStatement!=null)
//                    preparedStatement.close();
//            }catch(SQLException se2){
//            }// nothing we can do
//            try{
//                if(connection!=null)
//                    connection.close();
//            }catch(SQLException se){
//                se.printStackTrace();
//            }//end finally try
//        }//end try
//
//        return pizzas;
//    }

    public static ArrayList<Ingredient> getIngredientNameAndIdByPizzaId(int pizzaId)
    {

        ArrayList<Ingredient> ingredients=new ArrayList<Ingredient>();

        try{
            connection=DAOdatabase.sqlConnection();
            preparedStatement=connection.prepareStatement("SELECT  ingredient_id , name  from my_pizza_ingrediens inner join ingredients " +
                    "on my_pizza_ingrediens.ingredient_id = ingredients.ingredients_id where my_pizza_ingrediens.pizza_id=?");
            preparedStatement.setInt(1, pizzaId);

            ResultSet resultSet= preparedStatement.executeQuery();
            while (resultSet.next()){
                Ingredient ingredien=new Ingredient();
                ingredien.setName(resultSet.getString("name"));
                ingredien.setId(resultSet.getInt("ingredient_id"));
                ingredients.add(ingredien);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }finally{
            //finally block used to close resources
            try{
                if(preparedStatement!=null)
                    preparedStatement.close();
            }catch(SQLException se2){
            }// nothing we can do
            try{
                if(connection!=null)
                    connection.close();
            }catch(SQLException se){
                se.printStackTrace();
            }//end finally try
        }//end try

        return ingredients;

    }

    public static ArrayList<MyPizza> getAllCompletedPizza(){
        ArrayList<MyPizza> pizzaArrayList= new ArrayList<MyPizza>();


        try {


            connection = DAOdatabase.sqlConnection();
            preparedStatement = connection.prepareStatement("select pizza_id, name from my_pizza where name IS NOT NULL");
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                MyPizza myPizza=new MyPizza();
                myPizza.setId(resultSet.getInt("pizza_id"));
                myPizza.setName(resultSet.getString("name"));
               ArrayList<Ingredient> ingredients=getIngredientNameAndIdByPizzaId(myPizza.getId());
                myPizza.setIngredients(ingredients);

                pizzaArrayList.add(myPizza);


            }


        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            //finally block used to close resources
            try {
                if (preparedStatement != null)
                    preparedStatement.close();
            } catch (SQLException se2) {
            }// nothing we can do
            try {
                if (connection != null)
                    connection.close();
            } catch (SQLException se) {
                se.printStackTrace();
            }//end finally try
        }//end
        return pizzaArrayList;
    }

    public static ArrayList<Integer> getPizzaIngredientId(int pizzaId) throws Exception {
        ArrayList<Integer> ingredients_id=new ArrayList<Integer>();

        try{
            connection=DAOdatabase.sqlConnection();
            preparedStatement=connection.prepareStatement("select ingredients_id from my_pizza_ingrediens where pizza_id like ?");
            preparedStatement.setInt(1, pizzaId);

            ResultSet resultSet= preparedStatement.executeQuery();
            while (resultSet.next()){
                ingredients_id.add(resultSet.getInt("ingredients_id"));
            }

            } catch (SQLException e) {
                e.printStackTrace();
            }finally{
            //finally block used to close resources
            try{
                if(preparedStatement!=null)
                    preparedStatement.close();
            }catch(SQLException se2){
            }// nothing we can do
            try{
                if(connection!=null)
                    connection.close();
            }catch(SQLException se){
                se.printStackTrace();
            }//end finally try
        }//end try

            return ingredients_id;


    }


    public static String getPizzaNameById(int pizzaId){
        String pizzaName="";

        try {
            connection = DAOdatabase.sqlConnection();
            preparedStatement = connection.prepareStatement("select name from my_pizza_ingrediens where pizza_id=?");
            preparedStatement.setInt(1,pizzaId);
            ResultSet resultSet=preparedStatement.executeQuery();
            resultSet.next();
            pizzaName=resultSet.getString("name");

        } catch (SQLException e) {
            e.printStackTrace();
        }finally{
            //finally block used to close resources
            try{
                if(preparedStatement!=null)
                    preparedStatement.close();
            }catch(SQLException se2){
            }// nothing we can do
            try{
                if(connection!=null)
                    connection.close();
            }catch(SQLException se){
                se.printStackTrace();
            }//end finally try
        }//end try

        return pizzaName;
    }

    public static ArrayList<Integer> getAllComplitedPizzaId() {

        ArrayList<Integer> pizzaIds = new ArrayList<Integer>();

        try {
            connection = DAOdatabase.sqlConnection();
            preparedStatement = connection.prepareStatement("select pizza_id from my_pizza where name IS NOT NULL");
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                pizzaIds.add(resultSet.getInt("pizza_id"));

            }


        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            //finally block used to close resources
            try {
                if (preparedStatement != null)
                    preparedStatement.close();
            } catch (SQLException se2) {
            }// nothing we can do
            try {
                if (connection != null)
                    connection.close();
            } catch (SQLException se) {
                se.printStackTrace();
            }//end finally try
        }//end
        return pizzaIds;
    }


    public static ArrayList<Integer> getIngredientIdByPizzaId(int pizzaId){
        ArrayList<Integer> ingredientsIDs=new ArrayList<Integer>();
        try {
            connection = DAOdatabase.sqlConnection();
            preparedStatement = connection.prepareStatement("select ingredient_id from my_pizza_ingrediens where pizza_id=?");
            preparedStatement.setInt(1, pizzaId);
            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                ingredientsIDs.add(resultSet.getInt("ingredient_id"));

            }


        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            //finally block used to close resources
            try {
                if (preparedStatement != null)
                    preparedStatement.close();
            } catch (SQLException se2) {
            }// nothing we can do
            try {
                if (connection != null)
                    connection.close();
            } catch (SQLException se) {
                se.printStackTrace();
            }//end finally try
        }//end

        return ingredientsIDs;

    }






        public static ArrayList<String> getIngredientsName(ArrayList<Integer> ingredient_id){
        ArrayList<String> ingredientsName=new ArrayList<String>();
        try {
            for (int i=0; i<ingredient_id.size(); i++) {
                connection = DAOdatabase.sqlConnection();
                PreparedStatement preparedStatement = null;
                try {
                    preparedStatement = connection.prepareStatement("SELECT name from ingredients where ingredients_id=?");
                } catch (SQLException e) {
                    System.out.print("Error: PrepareStatement cant by created ");
                    e.printStackTrace();
                }
                preparedStatement.setInt(1,ingredient_id.get(i));
                ResultSet resultSet=preparedStatement.executeQuery();
                ingredientsName.add(resultSet.getString("name"));
            }

        } catch (SQLException e) {
          System.out.println("Error: SQL exception.");
            e.printStackTrace();
        }finally{
            //finally block used to close resources
            try{
                if(preparedStatement!=null)
                    preparedStatement.close();
            }catch(SQLException se2){
            }// nothing we can do
            try{
                if(connection!=null)
                    connection.close();
            }catch(SQLException se){
                se.printStackTrace();
            }//end finally try
        }//end try

        return  ingredientsName;
    }

    public static  ArrayList<Ingredient> getIngredients()  {
        ArrayList<Ingredient> ingredients=new ArrayList();

        try {

                connection = DAOdatabase.sqlConnection();
                PreparedStatement preparedStatement = null;
                try {
                    preparedStatement = connection.prepareStatement("SELECT * from ingredients");
                } catch (SQLException e) {
                    System.out.print("Error: PrepareStatement cant by created ");
                    e.printStackTrace();
                }

                ResultSet resultSet=preparedStatement.executeQuery();

            while (resultSet.next()) {
                Ingredient ingredient=new Ingredient();
                ingredient.setId(resultSet.getInt("ingredients_id"));
                ingredient.setName(resultSet.getString("name"));
                ingredient.setPrice(resultSet.getFloat("price"));

                ingredients.add(ingredient);
            }

        } catch (SQLException e) {
            System.out.println("Error: SQL exception.");
            e.printStackTrace();
        }finally{
            //finally block used to close resources
            try{
                if(preparedStatement!=null)
                    preparedStatement.close();
            }catch(SQLException se2){
            }// nothing we can do
            try{
                if(connection!=null)
                    connection.close();
            }catch(SQLException se){
                se.printStackTrace();
            }//end finally try
        }//end try




        return  ingredients;
    }


    public static HashMap<String, Float> getAllIngredientsAndPrice() {
        HashMap<String, Float> ingredienAndPrice = new HashMap<String, Float>();


            connection = DAOdatabase.sqlConnection();
            PreparedStatement preparedStatement = null;
            try {
                preparedStatement = connection.prepareStatement("SELECT name , price FROM ingredients");
            } catch (SQLException e) {
                System.out.print("Error: PrepareStatement cant by created ");
                e.printStackTrace();
            }
            ResultSet resultSet = null;
            try {
                resultSet = preparedStatement.executeQuery();


                while (resultSet.next()) {
                    String name = resultSet.getString("name");
                    Float price = resultSet.getFloat("price");
                    System.out.println(name + price);
                    ingredienAndPrice.put(name, price);
                }
            } catch (SQLException e) {
                System.out.println("Error: SQL exception.");
                e.printStackTrace();
            }finally{
                //finally block used to close resources
                try{
                    if(preparedStatement!=null)
                        preparedStatement.close();
                }catch(SQLException se2){
                }// nothing we can do
                try{
                    if(connection!=null)
                        connection.close();
                }catch(SQLException se){
                    se.printStackTrace();
                }//end finally try
            }//end try




        return ingredienAndPrice;
    }


    public static Ingredient getIngredientById(int ingredId)  {
        Ingredient ingredient=new Ingredient();


        try {

            connection = DAOdatabase.sqlConnection();
            PreparedStatement preparedStatement = null;
            try {
                preparedStatement = connection.prepareStatement("SELECT * from ingredients where ingredients_id=?");
            } catch (SQLException e) {
                System.out.print("Error: PrepareStatement cant by created ");
                e.printStackTrace();
            }
            preparedStatement.setInt(1,ingredId);
            ResultSet resultSet=preparedStatement.executeQuery();
            resultSet.next();

                ingredient.setId(resultSet.getInt("ingredients_id"));
                ingredient.setName(resultSet.getString("name"));
                ingredient.setPrice(resultSet.getFloat("price"));



        } catch (SQLException e) {
            System.out.println("Error: SQL exception.");
            e.printStackTrace();
        }finally{
            //finally block used to close resources
            try{
                if(preparedStatement!=null)
                    preparedStatement.close();
            }catch(SQLException se2){
            }// nothing we can do
            try{
                if(connection!=null)
                    connection.close();
            }catch(SQLException se){
                se.printStackTrace();
            }//end finally try
        }//end try

        return  ingredient;
    }

//    public static ArrayList<Ingredient> getIngredientsByIds(ArrayList<Integer> ingredient_id){
//        ArrayList<Ingredient> ingredients=new ArrayList<Ingredient>();
//        try {
//            for (int i=0; i<ingredient_id.size(); i++) {
//                connection = DAOdatabase.sqlConnection();
//                PreparedStatement preparedStatement = null;
//                try {
//                    preparedStatement = connection.prepareStatement("SELECT * from ingredients where ingredients_id=?");
//                } catch (SQLException e) {
//                    System.out.print("Error: PrepareStatement cant by created ");
//                    e.printStackTrace();
//                }
//                preparedStatement.setInt(1,ingredient_id.get(i));
//                ResultSet resultSet=preparedStatement.executeQuery();
//                while (resultSet.next()) {
//                    Ingredient ingredient=new Ingredient();
//                    ingredient.setId(resultSet.getInt("ingredients_id"));
//                    ingredient.setName(resultSet.getString("name"));
//                    ingredient.setPrice(resultSet.getFloat("price"));
//
//                    ingredients.add(ingredient);
//                }
//            }
//
//        } catch (SQLException e) {
//            System.out.println("Error: SQL exception.");
//            e.printStackTrace();
//        }finally{
//            //finally block used to close resources
//            try{
//                if(preparedStatement!=null)
//                    preparedStatement.close();
//            }catch(SQLException se2){
//            }// nothing we can do
//            try{
//                if(connection!=null)
//                    connection.close();
//            }catch(SQLException se){
//                se.printStackTrace();
//            }//end finally try
//        }//end try
//
//        return  ingredients;
//    }


    public static float getPizzaBasePriceByIdSize(int sizeId){

        float price=0;

        try {

            connection = DAOdatabase.sqlConnection();
            PreparedStatement preparedStatement = null;
            try {
                preparedStatement = connection.prepareStatement("SELECT price from pizza_size where pizza_size_id=?");
            } catch (SQLException e) {
                System.out.print("Error: PrepareStatement cant by created ");
                e.printStackTrace();
            }
            preparedStatement.setInt(1,sizeId);
            ResultSet resultSet=preparedStatement.executeQuery();
            resultSet.next();
            price=resultSet.getFloat("price");

        } catch (SQLException e) {
            System.out.println("Error: SQL exception.");
            e.printStackTrace();
        }finally{
            //finally block used to close resources
            try{
                if(preparedStatement!=null)
                    preparedStatement.close();
            }catch(SQLException se2){
            }// nothing we can do
            try{
                if(connection!=null)
                    connection.close();
            }catch(SQLException se){
                se.printStackTrace();
            }//end finally try
        }//end try

        return  price;
    }

    public static MyPizza getCompletedPizzaById(int pizzaId){
        MyPizza pizza= new MyPizza();


        try {


            connection = DAOdatabase.sqlConnection();
            preparedStatement = connection.prepareStatement("SELECT name from my_pizza WHERE pizza_id=?");
            preparedStatement.setInt(1, pizzaId);
            ResultSet resultSet = preparedStatement.executeQuery();

                resultSet.next();

                pizza.setId(pizzaId);
                pizza.setName(resultSet.getString("name"));




            try {
                preparedStatement = connection.prepareStatement("SELECT ingredients_id, name, price  from my_pizza_ingrediens " +
                        "as ing inner join ingredients as p ON ingredients_id=ingredient_id WHERE ing.pizza_id=?");
            } catch (SQLException e) {
                System.out.print("Error: PrepareStatement cant by created ");
                e.printStackTrace();
            }
            preparedStatement.setInt(1,pizzaId);
            resultSet=preparedStatement.executeQuery();

            List<Ingredient> ingredients=new ArrayList<Ingredient>();

            while (resultSet.next()) {
                Ingredient ingredient=new Ingredient();
                ingredient.setId(resultSet.getInt(1));
                ingredient.setName(resultSet.getString("name"));
                ingredient.setPrice(resultSet.getFloat("price"));

                ingredients.add(ingredient);
            }

            pizza.setIngredients(ingredients);




        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            //finally block used to close resources
            try {
                if (preparedStatement != null)
                    preparedStatement.close();
            } catch (SQLException se2) {
            }// nothing we can do
            try {
                if (connection != null)
                    connection.close();
            } catch (SQLException se) {
                se.printStackTrace();
            }//end finally try
        }//end
        return pizza;
    }






}


